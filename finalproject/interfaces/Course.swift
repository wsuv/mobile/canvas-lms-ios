//
//  Course.swift
//  finalproject
//
//  Created by John Karasev on 4/23/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation

struct Course: Codable {
    var id: Int
    var accountId: Int
    var name: String
    
    enum Keys: String, CodingKey {
        case id
        case accountId = "account_id"
        case name
    }
    
    init() {
        id = -1
        accountId = -1
        name = ""
    }
    
    init(from decoder: Decoder) throws {
        let valueContainer = try decoder.container(keyedBy: Keys.self)
        self.name = try valueContainer.decode(String.self, forKey: Keys.name)
        self.id = try valueContainer.decode(Int.self, forKey: Keys.id)
        self.accountId = try valueContainer.decode(Int.self, forKey: Keys.accountId)
    }
}




