//
//  Announcement.swift
//  finalproject
//
//  Created by John Karasev on 4/23/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation

struct Announcment: Codable {
    var id: Int
    var title: String
    var description: String
    var userName: String
    var position: Int
    var postedAt: Date
    
    enum Keys: String, CodingKey {
        case id
        case title
        case description = "message"
        case postedAt = "posted_at"
        case userName = "user_name"
        case position
    }
    
    init() {
        id = -1
        title = ""
        description = ""
        userName = ""
        position = 0
        postedAt = Date()
    }
    
    init(from decoder: Decoder) throws {
        let valueContainer = try decoder.container(keyedBy: Keys.self)
        self.id = try valueContainer.decode(Int.self, forKey: Keys.id)
        self.title = try valueContainer.decode(String.self, forKey: Keys.title)
        self.description = try valueContainer.decode(String.self, forKey: Keys.description)
        self.userName = try valueContainer.decode(String.self, forKey: Keys.userName)
        self.position = try valueContainer.decode(Int.self, forKey: Keys.position)
        
        
        if let dateStr = try? valueContainer.decode(String.self, forKey: Keys.postedAt) {
            self.postedAt = Date(fromUTC: dateStr)
        } else {
            self.postedAt = Date()
        }
    }
    
}
