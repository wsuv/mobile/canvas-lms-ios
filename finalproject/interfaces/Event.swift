//
//  Event.swift
//  finalproject
//
//  Created by John Karasev on 4/23/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation

struct Event: Codable {
    var title: String
    var description: String
    var date: Date
    
    enum Keys: String, CodingKey {
        case title
        case description
        case date = "start_at"
    }
    
    init() {
        title = ""
        description = ""
        date = Date()
    }
    
    init(from decoder: Decoder) throws {
        let valueContainer = try decoder.container(keyedBy: Keys.self)
        self.title = try valueContainer.decode(String.self, forKey: Keys.title)
        self.description = try valueContainer.decode(String.self, forKey: Keys.description)
        if let dateStr = try? valueContainer.decode(String.self, forKey: Keys.date) {
            self.date = Date(fromUTC: dateStr)
        } else {
            self.date = Date()
        }
    }

}
