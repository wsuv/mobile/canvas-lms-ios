//
//  Assignment.swift
//  finalproject
//
//  Created by John Karasev on 4/23/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation

struct Assignment: Codable {
    var id: Int
    var name: String
    var description: String
    var dueAt: Date
    var pointsPossible: Int
    var isSubmitted: Bool
    var isQuiz: Bool
    
    enum Keys: String, CodingKey {
        case id
        case name
        case description
        case dueAt = "due_at"
        case pointsPossible = "points_possible"
        case isSubmitted = "has_submitted_submissions"
        case isQuiz = "is_quiz_assignment"
    }
    
    init() {
        id = -1
        name = ""
        description = ""
        dueAt = Date()
        pointsPossible = 0
        isSubmitted = false
        isQuiz = false
    }
    
    init(from decoder: Decoder) throws {
        let valueContainer = try decoder.container(keyedBy: Keys.self)
        self.id = try valueContainer.decode(Int.self, forKey: Keys.id)
        self.name = try valueContainer.decode(String.self, forKey: Keys.name)
        self.description = try valueContainer.decode(String.self, forKey: Keys.description)
        self.pointsPossible = try valueContainer.decode(Int.self, forKey: Keys.pointsPossible)
        self.isSubmitted = try valueContainer.decode(Bool.self, forKey: Keys.isSubmitted)
        self.isQuiz = try valueContainer.decode(Bool.self, forKey: Keys.isQuiz)
        if let dateStr = try? valueContainer.decode(String.self, forKey: Keys.dueAt) {
            self.dueAt = Date(fromUTC: dateStr)
        } else {
            self.dueAt = Date()
        }
       
    }
}
