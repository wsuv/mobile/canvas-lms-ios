//
//  CanvasController.swift
//  finalproject
//
//  Created by John Karasev on 4/23/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation


struct CanvasController {
    
    let base: URL = URL(string: "https://udg.education/api/v1")!;
    let key: String = "9G0RAkHl7lMe0NkpOZrrXg3JBo6jkAbyTlYYygVg1127wJfTwpBF6iPpUrHpE4tS";
    
    func fetch<T: Decodable>(resource: String, queryDict: [String: String]? = nil, completion: @escaping (T?) -> Void) {
        var url = base.appendingPathComponent(resource)
    
        if let queryDict = queryDict {
            url = url.withQueries(queryDict)!
        }
        var request = URLRequest(url: url)
        request.addValue("Bearer " + key, forHTTPHeaderField: "Authorization")

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            let jsonDecoder = JSONDecoder()
            if let data = data,
                let result = try?
                    jsonDecoder.decode(T.self, from: data) {
                
                completion(result)
            } else {
                //Either no data was returned, or data was not serialized.
                completion(nil)
            }
        }
        task.resume()

    }
}
