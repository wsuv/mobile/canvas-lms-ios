//
//  InfoPopView.swift
//  finalproject
//
//  Created by John Karasev on 4/23/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation
import UIKit

class ModalInfoView: UIViewController {
    
    @IBOutlet weak var eventTitle: UITextField!
    @IBOutlet weak var eventDate: UITextField!
    @IBOutlet weak var eventDescription: UITextView!
    
    var event = Event()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventTitle.text = event.title
        eventDate.text = event.date.formatDate()
        eventDescription.text = event.description.withoutHtmlTags
        // Do any additional setup after loading the view.
    }
    
    @IBAction func disapear(_ sender: Any) {
        dismiss(animated: true)
    }
    
}
