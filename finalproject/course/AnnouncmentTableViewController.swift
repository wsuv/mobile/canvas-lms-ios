//
//  AnnouncmentTableViewController.swift
//  finalproject
//
//  Created by John Karasev on 4/23/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import UIKit

class AnnouncmentTableViewController: UITableViewController {
    
    var announcments: [Announcment] = []
    var courseId = 1
    let canvasController = CanvasController()
    var selected = Announcment()

    override func viewDidLoad() {
        super.viewDidLoad()
        let tabbar = tabBarController as! CourseTabBarView
        courseId = tabbar.course.id
        fetchAnnouncments()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return announcments.count
        } else {
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnnouncmentCell", for: indexPath) as! TableViewCell
        let announcment = announcments[indexPath.row]
        cell.cellTitle.text = announcment.title
        cell.cellUserName.text = announcment.userName
        cell.cellDate.text = announcment.postedAt.formatDate()
        cell.cellDescription.text = announcment.description.withoutHtmlTags
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let preview = segue.destination as? AnnouncementPreviewController,
            let index = tableView.indexPathForSelectedRow?.row
            else {return}
        preview.announcement = self.announcments[index]
    }
    
    @IBAction func refreshData(_ sender: Any) {
        fetchAnnouncments()
    }
    
    @IBAction func updateSelected(_ sender: UIButton) {
        
    }
    
    func fetchAnnouncments() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        canvasController.fetch(resource: "announcements", queryDict: ["context_codes[]":
        "course_\(courseId)"]) { (announcments: [Announcment]?) in
            DispatchQueue.main.async {
                if let announcments = announcments {
                    self.announcments = announcments
                    self.tableView.reloadData()
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    let alert = UIAlertController(title: "Error", message: "failed to fetch announcements", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
}

class TableViewCell: UITableViewCell {
    @IBOutlet weak var cellTitle: UITextField!
    @IBOutlet weak var cellUserName: UITextField!
    @IBOutlet weak var cellDate: UITextField!
    @IBOutlet weak var cellDescription: UITextView!
}
