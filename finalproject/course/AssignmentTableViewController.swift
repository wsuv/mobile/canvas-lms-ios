//
//  AssignmentView.swift
//  finalproject
//
//  Created by John Karasev on 4/23/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation
import UIKit

class AssignmentTableViewController: UITableViewController {
    
    var assignments: [Assignment] = []
    var courseId = 1
    let canvasController = CanvasController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tabbar = tabBarController as! CourseTabBarView
        courseId = tabbar.course.id
        fetchAssignments()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return assignments.count
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AssignmentCell", for: indexPath)
        let assignment = assignments[indexPath.row]
        cell.textLabel?.text = assignment.name
        cell.detailTextLabel?.text = assignment.dueAt.formatDate()
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let preview = segue.destination as? AssignmentPreviewViewController,
            let index = tableView.indexPathForSelectedRow?.row
            else {return}
        preview.assignment = self.assignments[index]
    }
    
    @IBAction func refreshData(_ sender: Any) {
        fetchAssignments()
    }
    
    func fetchAssignments() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        canvasController.fetch(resource: "courses/\(courseId)/assignments" ) { (assignments: [Assignment]?) in
            DispatchQueue.main.async {
                if let assignments = assignments {
                    self.assignments = assignments
                    self.tableView.reloadData()
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    let alert = UIAlertController(title: "Error", message: "failed to fetch assignments", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
    
}
