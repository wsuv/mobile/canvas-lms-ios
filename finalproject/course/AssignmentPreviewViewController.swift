//
//  AssignmentPreviewViewController.swift
//  finalproject
//
//  Created by John Karasev on 4/23/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import UIKit

class AssignmentPreviewViewController: UIViewController {
    @IBOutlet weak var assignmentName: UITextField!
    @IBOutlet weak var assignmentDueDate: UITextField!
    @IBOutlet weak var assignmentDescription: UITextView!
    
    var assignment = Assignment()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        assignmentName.text = assignment.name
        assignmentDueDate.text = assignment.dueAt.formatDate()
        assignmentDescription.text = assignment.description.withoutHtmlTags

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func disapear(_ sender: Any) {
        dismiss(animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
