//
//  AnnouncementPreviewController.swift
//  finalproject
//
//  Created by John Karasev on 4/24/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import UIKit

class AnnouncementPreviewController: UIViewController {
    @IBOutlet weak var announcementTitle: UITextField!
    @IBOutlet weak var user: UITextField!
    @IBOutlet weak var date: UITextField!
    @IBOutlet weak var announcementDescription: UITextView!
    
    var announcement = Announcment()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        announcementTitle.text = announcement.title
        user.text = announcement.userName
        date.text = announcement.postedAt.formatDate()
        announcementDescription.text = announcement.description.withoutHtmlTags

        // Do any additional setup after loading the view.
    }
    
    @IBAction func disapear(_ sender: Any) {
        dismiss(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
