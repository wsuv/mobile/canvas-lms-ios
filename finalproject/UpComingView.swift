//
//  UpCommingView.swift
//  finalproject
//
//  Created by John Karasev on 4/23/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation
import UIKit

class UpComingView: UITableViewController {
    
    var events: [Event] = []
    let canvasController = CanvasController()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchUpcomming()
    }
    
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Returns the number of rows in the given section
        // Sections and rows index from 0
        if section == 0 {
            return events.count
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // sets up a cell that's been requested for display
        // An IndexPath includes a section and a row. Both index from 0
        
        // Set the identifier in Interface Builder
        // This grabs a cell from a reusable pool, so all we have to do is populate it
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath)
        
        // We do this in IB, but you can set it here, instead
        //cell.showsReorderControl = true
        
        // we map table rows directly to array entries, so this
        // fetches the relevent emoji so we can use it.
        // (this is handy for safety, and simplifies references)
        let event = events[indexPath.row]
        
        // populate text in the cell label and detail
        cell.textLabel?.text = event.title
        
        cell.detailTextLabel?.text = event.date.formatDate()
        
        // kick back prepared cell
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let modalView = segue.destination as? ModalInfoView,
            let index = tableView.indexPathForSelectedRow?.row
            else {return}
        modalView.event = self.events[index]
    }
    
    @IBAction func refreshData(_ sender: UIBarButtonItem) {
        fetchUpcomming()
    }
    
    func fetchUpcomming() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        canvasController.fetch(resource: "users/self/upcoming_events") { (events: [Event]?) in
            
            DispatchQueue.main.async {
                if let events = events {
                    self.events = events
                    self.tableView.reloadData()
                } else {
                    let alert = UIAlertController(title: "Error", message: "failed to fetch upcommming events", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
    
    
}
