//
//  DateHelper.swift
//  finalproject
//
//  Created by John Karasev on 4/23/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation

extension Date {
    
    func formatDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .medium
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter.string(from: self)
    }
    
    init(fromUTC: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        self = dateFormatter.date(from: fromUTC)!
    }
}
