//
//  CourseView.swift
//  finalproject
//
//  Created by John Karasev on 4/23/19.
//  Copyright © 2019 John Karasev. All rights reserved.
//

import Foundation
import UIKit

class CoursesTableView: UITableViewController {
    
    let canvasController = CanvasController()
    
    var courseList: [Course] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.title = "COURSES"
        fetchCourses()
        // Do any additional setup after loading the view.
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Returns the number of rows in the given section
        // Sections and rows index from 0
        if section == 0 {
            return courseList.count
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // sets up a cell that's been requested for display
        // An IndexPath includes a section and a row. Both index from 0
        
        // Set the identifier in Interface Builder
        // This grabs a cell from a reusable pool, so all we have to do is populate it
        let cell = tableView.dequeueReusableCell(withIdentifier: "CourseCell", for: indexPath)
        
        // We do this in IB, but you can set it here, instead
        //cell.showsReorderControl = true
        
        // we map table rows directly to array entries, so this
        // fetches the relevent emoji so we can use it.
        // (this is handy for safety, and simplifies references)
        let course = courseList[indexPath.row]
        
        // populate text in the cell label and detail
        cell.textLabel?.text = course.name
        
        // kick back prepared cell
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let courseInfoView = segue.destination as? CourseTabBarView,
            let index = tableView.indexPathForSelectedRow?.row
            else {return}
        courseInfoView.course = self.courseList[index]
    }
    
    
    @IBAction func refresh(_ sender: Any) {
        fetchCourses()
    }
    
    
    func fetchCourses() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        canvasController.fetch(resource: "courses" ) { (courses: [Course]?) in
            
            DispatchQueue.main.async {
                if let courses = courses {
                    self.courseList = courses
                    self.tableView.reloadData()
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    let alert = UIAlertController(title: "Error", message: "failed to fetch courses", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
}

